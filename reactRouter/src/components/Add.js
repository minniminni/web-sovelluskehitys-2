import React, {useEffect, useState} from 'react';
import NoteForm from './NoteForm'
import Notification from './Notification'
import noteService from './services/notes'

const Add = () => {
  const [notes, setNotes] = useState([])
  const [newContent, setNewContent ] = useState('')
  const [newDate, setNewDate] = useState('')
  const [message, setMessage] = useState(null) //success

  //Haetaan data palvelimelta
  useEffect(() => {
    noteService
    .getAll()
    .then(initialNotes => {
      setNotes(initialNotes)
    })
  }, [])

  //Lisää henkilö (+ ehdot onnistuneeseen lisäykseen)
  const addNote = (event) =>{
    event.preventDefault()
    const noteObject = {content: newContent, date: newDate}

    if(noteObject.content == "" || noteObject.date == ""){
      setMessage(`Täytä puuttuvat kentät!`)
      setTimeout(() => {
        setMessage(null)
      }, 3000)
    }else{
      noteService
      .create(noteObject)
      .then(returnedNote => {
        setMessage(`"${noteObject.content} ja ${noteObject.date}" Tiedot lähetetty!`)
        setTimeout(() => {
          setMessage(null)
        }, 3000)
        setNotes(notes.concat(returnedNote))
        setNewContent('')
        setNewDate('')
      })
      .catch((error) =>
          // pääset käsiksi palvelimen palauttamaan virheilmoitusolioon näin
          setMessage(error.response.data.error)
      )
      setTimeout(() => {
        setMessage(null)
      }, 4000)
    }
  }

  //Synkronoi syötekenttiin tehdyt muutokset
  const handleContentChange= (event) =>{
    setNewContent(event.target.value)
  }

  const handleDateChange = (event) =>{
    setNewDate(event.target.value)
  }

  return(
        <div className="container">
           <div>
             <Notification message={message}/>
             <h4>Add new</h4>
             <div>
             <NoteForm add ={addNote} content={newContent} date={newDate} handleContent={handleContentChange} handleDate={handleDateChange}/>
             </div>
           </div>
        </div>
    )
}

export default Add